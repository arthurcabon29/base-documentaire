//node object to tab
function nodelisteToArray (monObjet) {
    if(monObjet !== undefined)  {
        return Object.keys(monObjet).map(function(cle) {
            return monObjet[cle];
        });
    } else {
        return false;
    }
}

function addCat (tab, currentCat, niveau) {
    //séléctionner le template
    let template = document.querySelector('#tmltArbo');
    if (tab.length > 0) {
        //seletionner la catégirie si celle ci existe
        let catExist = nodelisteToArray(currentCat.querySelectorAll('.namecat')).find(elem => {return elem.innerText == tab[0] && elem.niveau == (niveau)});
        if(catExist !== undefined) {
            addCat(tab.slice(1), catExist.nextElementSibling, ++niveau);
        } else {
            //créer l'élément via le template si la cat n'existe pas
            var clone = document.importNode(template.content, true);
            clone.querySelector(".namecat").innerText = tab[0];
            clone.querySelector(".namecat").niveau = niveau;
            clone.querySelector(".namecat").addEventListener('click', () => {
                document.querySelector('input[name=arborescence]').value = tab[0];
                document.querySelector('input[name=niveauArbo]').value = niveau;
                document.querySelector('#recherche').submit()
            });
            currentCat.appendChild(clone);
            addCat(tab.slice(1), nodelisteToArray(currentCat.querySelectorAll('.namecat')).find(elem => {return elem.innerText == tab[0] && elem.niveau == (niveau)}).nextElementSibling, ++niveau);
        }
    }
}

//demande un tabeau de chaine
function transTab (tab) {
    //selection de l'arborescence 
    const arborescence = document.querySelector('.arborescence');

    tab.forEach(element => {
        addCat(element.split('/'), arborescence, 0)
    });
    
}






let form = new FormData();
fetch('../autres/routes.php', { method: 'POST', body: form, credentials: 'same-origin' })
    .then(function(response) {
        return response.json();
    })
    .then(function(jsonRoutes) {
        transTab(jsonRoutes);
        //déplier/replier une catégorie au clic
        document.querySelectorAll('.arborescence .arrow').forEach((arrow) => {
        if(arrow.nextElementSibling.nextElementSibling.querySelector('.cat')) {
            arrow.addEventListener('click', () => {
                arrow.nextElementSibling.nextElementSibling.classList.toggle('unfold');
                arrow.classList.toggle('oppen');
            });
        } else {
            arrow.style.display = 'none';
        }
    });
});