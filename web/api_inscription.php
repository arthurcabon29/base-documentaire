<?php


//api d'inscription à mettre dans le document d'api
$app->post('/inscription', function ($req, $resp, $args) {

	global $mysqlConnection;
    $params = $req->getParsedBody();
    $nom = htmlspecialchars($params['nom']);
    $prenom = htmlspecialchars($params['prenom']);
    $Email = htmlspecialchars($params['Email']);
    $pswd = htmlspecialchars($params['pswd']);
    $pswdConfirm = htmlspecialchars($params['pswdConfirm']);
    
    $role = htmlspecialchars($params['role']);
    $entreprise = htmlspecialchars($params['entreprise']);

    $recipesStatement = $mysqlConnection->prepare('SELECT * FROM `_user` WHERE iduser=:iduser');
    $recipesStatement->execute(array(':iduser'=> $_SESSION['idUser']));
    $user = $recipesStatement->fetch(PDO::FETCH_ASSOC);
    if( !isset($entreprise) || $entreprise === false ) {
        $entreprise = $user["identreprise"];
    }
    if( !isset($role) || $role === false ) {
        $role = $user['idrole'] + 1;
    }

    if (!empty($nom) && !empty($prenom) && !empty($Email) && !empty($pswd) && !empty($pswdConfirm) && !empty($entreprise) && !empty($role) && $pswd === $pswdConfirm) {
        //crypter mot de passe
        $recipesStatement = $mysqlConnection->prepare("INSERT INTO `_user`(`idrole`, `identreprise`, `nom`, `prenom`, `email`, `motdepasse`) VALUES (':idrole',':identreprise',':nom',':prenom',':email',':motdepasse')");
        //INSERT INTO `_user`(`idrole`, `identreprise`, `nom`, `prenom`, `email`, `motdepasse`) VALUES ('[value-2]','[value-3]','[value-4]','[value-5]','[value-6]','[value-7]')
		$recipesStatement->execute(array(':idrole'=> $role, ':identreprise'=> $entreprise,':nom'=> $nom,':prenom'=> $prenom,':email'=> $Email,':motdepasse'=> $pswd));
    } else {
        return $resp->withStatus(401);
    }

    return $resp->withStatus(200);
});