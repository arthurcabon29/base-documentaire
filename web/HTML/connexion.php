<?php

//////////////////////////////////////////////////////////////////////////////////////////
/* AUTHENTIFICATION TEMPORAIRE (NE PASSE PAS PAR API - TOKEN A VENIR) */

session_start();
// TODO : notif erreur si echec connexion
if (isset($_POST['Email']) && isset($_POST['Password'])) {
    $mail = htmlspecialchars($_POST['Email']);
    $pwd = htmlspecialchars($_POST['Password']);

    try {
        // Informations d'identification et connexion à la bdd
        $dsn = 'mysql:dbname=info_doc;host=baptiste.lpweb-lannion.fr:3306;charset=utf8';
        $dbUsername = 'info_doc';
        $dbPwd = 'gmB/^F7H>(7vc;jl';
        $pdo = new pdo($dsn, $dbUsername, $dbPwd);

        $checkUser = "SELECT * FROM _user WHERE email='$mail' and motdepasse='$pwd'";
        $resCheckUser = $pdo->query($checkUser)->fetch(PDO::FETCH_ASSOC);

        if ($resCheckUser !== null) {
            $_SESSION['username'] = $mail;
            $_SESSION['idrole'] = $resCheckUser['idrole'];
            $_SESSION['identreprise'] = $resCheckUser['identreprise'];
            header('Location: home.php');
        }
    } catch (Exception $e) {
        print("'\n Echec fonction login : " . $e->getMessage());
    }
}
////////////////////////////////////////////////////////////////////////////////////////// 

include('header.php');
?>

<main id="cnx">
    <!-- <div class="col-xs-12 col-md-9 col-lg-6 col-xl-4"> -->
    <div class="container">
        <form action="connexion.php" method="POST" class="form_id">
            <!-- <form action="index.php?action=login" method="POST" class="form_id"> -->

            <h1 class="fw-bold mb-3">Se connecter</h1>

            <div class="form-floating mb-3">
                <input type="email" class="form-control" id="Email" name="Email" placeholder="nomprenom@example.com" required>
                <label for="Email">Email</label>
            </div>
            <div class="form-floating">
                <input type="password" class="form-control" id="Password" name="Password" placeholder="Password" required><i onclick="pswdVisible()" id="eye" class="toggle-pwd far fa-eye-slash"></i>
                <label for="Password">Mot de passe</label>
            </div>

            </br>
            <input type="submit" value="Connexion" class="btn btn-dark btn-lg form-control">
            <hr>
            <a href="changer_mdp.php">Mot de passe oublié ?</a>

        </form>
    </div>

</main>

<script>
    /* --- Visibilité mot de passe --- */
    function pswdVisible() {
        var champ = document.getElementById("Password");

        if (champ.type === "password") {
            champ.type = "text";
            eye.className = "toggle-pwd far fa-eye";
        } else {
            champ.type = "password";
            eye.className = "toggle-pwd far fa-eye-slash";
        }
    }
</script>


<?php include('footer.php'); ?>