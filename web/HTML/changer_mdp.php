<?php include('header.php'); ?>

<main class="container col-xs-12 col-md-9 col-lg-6 col-xl-4">

<!-- <div class="position-absolute top-50 start-50 translate-middle"> -->

<form action="" class="form_id">

    <h1 class="fw-bold mb-3">Modifier votre mot de passe</h1>

    <div class="form-floating mb-3">
        <input type="password" name="current_pswd" class="form-control" id="floatingInput1" placeholder="Mot de passe actuel"><i onclick="pswdVisible(1)" id="eye1" class="toggle-pwd far fa-eye-slash"></i>
        <label for="floatingInput1">Mot de passe actuel</label>
    </div>

    <div class="form-floating mb-3">
        <input type="password" name="new_pswd" class="form-control" id="floatingInput2" placeholder="Nouveau mot de passe"><i onclick="pswdVisible(2)" id="eye2" class="toggle-pwd far fa-eye-slash"></i>
        <label for="floatingInput2">Nouveau mot de passe</label>
    </div>

    <div class="form-floating mb-3">
        <input type="password" name="confirm_new_pswd" class="form-control" id="floatingInput3" placeholder="Confirmer le nouveau mot de passe"><i onclick="pswdVisible(3)" id="eye3" class="toggle-pwd far fa-eye-slash"></i>
        <label for="floatingInput3">Confirmez le nouveau mot de passe</label>
    </div>


    <input type="submit" value="Modifier" class="btn btn-dark btn-lg form-control">

</form>

</div>

<!--</main>-->

<script>
    /* --- Visibilité mot de passe --- */
    function pswdVisible(param) {

        switch (param) {
            case 1:
                var champ = document.getElementById("floatingInput1");
                var eye = document.getElementById("eye1");

                break;
            case 2:
                var champ = document.getElementById("floatingInput2");
                var eye = document.getElementById("eye2");

                break;
            case 3:
                var champ = document.getElementById("floatingInput3");
                var eye = document.getElementById("eye3");

                break;
        }


        if (champ.type === "password") {
            champ.type = "text";
            eye.className = "toggle-pwd far fa-eye";
        } else {
            champ.type = "password";
            eye.className = "toggle-pwd far fa-eye-slash";
        }
    }
</script>


<?php include('footer.php'); ?>