<?php include('header.php');

?>

<link rel="stylesheet" type="text/css" href="../CSS/arborescence.css"/>

<main class="container" id="search">

    <div class="row">

        <div id="arbo" class="col-3">

            <div class="carte mb-5">
                <h5><strong>Ajouter un tag</strong></h5>
                
                <div class="position-relative">
                    <input type='text' class='searchtags' />
                    
                    <div id='tags'>
                        <?php


                            foreach($tabTag as $tag) {
                                echo '<p>'.$tag.'</p>';
                            }
                        ?>
                    </div>
                </div>

                <h6>Tags ajoutés</h6>

                <div class="tags research">
                    
                </div>
            </div>

            <div id="arbo" class="carte">
                <a href="#">Retour</a>
                
                <template id="tmltArbo">
                <li class="cat">
                    <span class="arrow"></span>
                    <span class="namecat"></span>
                    <ul class="sous-cat"></ul>
                </li>
                </template>
                <ul class="arborescence"></ul>
            </div>
            
        </div>

        <div class="col-9">
        
            <div class="tags tagsRecommended mb-5 mt-3">
                <h5>Tags principaux</h5>

                <button>Manuel</button>
                <button>Ordinateur</button>
                <button>Imprimante</button>
                <button>Appareils photos</button>
                <button>Téléphone</button>
            </div>

            <div>
                <form action="index.php?action=search" method="POST" class="row" id="recherche">

                    <?php 
                    if($param != ""){
                        ?>
                            <h4 class="col">Résultats pour : <strong id="param_recherche"><?php echo $param ?></strong></h4>
                        <?php
                    }
                    ?>
                    <input class="col" type="text" placeholder="Rechercher ..." name="param">
                    <input name="arborescence" type="hidden" value="">
                    <input name="niveauArbo" type="hidden" value="">
                </form>

                <!-- A boucler avec un foreach -->
                <table class="table table-hover mt-3">
                <tbody>

                    <?php
                    if($documents){
                    foreach($documents as $doc) :
                        $doc->size = $doc->size / 1000; ?>
<!--                         
                        <tr class="" title="tags : <?php $slugs = [];
                            foreach (explode(':', $doc->listeslug) as $value) {
                                echo $tabTag[$value].', ';
                            } ?>">
                            <th id="tab_nom"><?php echo $doc->nom ?></th>
                            <td id="tab_chemin"><?php echo $doc->chemin ?></td>
                            <th id="tab_taille"><?php echo $doc->size ?> ko</th>
                            <td id="tab_modifier"><a href=''><i class='fas fa-edit fs-3'></i></a></td>
                            <td id="tab_telecharger"><a href=''><i class='redIcon fas fa-cloud-download-alt fs-3'></i></a></td>
                        </tr>
                        <?php 
                    //endforeach;
                    ?> -->
                            <tr <?php 

                            if(count(explode(':', $doc->listeslug)) > 1) {
                                echo 'title="tags : ';
                                $slugs = [];
                                foreach (explode(':', $doc->listeslug) as $id => $value) {
                                    if($id != 0) echo ', ';
                                    echo $tabTag[$value];
                                }
                                echo '"';
                            } ?>>
                                <th id="tab_nom"><?php echo $doc->nom ?></th>
                                <td id="tab_chemin"><?php echo $doc->chemin ?></td>
                                <th id="tab_taille"><?php echo $doc->size ?> ko</th>
                                <td id="tab_modifier"><a href='document_update_form.php?param=<?php echo $doc->iddocument ?>'><i class='fas fa-edit fs-3'></i></a></td>
                                <td id="tab_telecharger"><a href='index.php?action=download_document&param=<?php echo $doc->iddocument ?>'><i class='redIcon fas fa-cloud-download-alt fs-3'></i></a></td>
                            </tr>
                    <?php endforeach; 
                    }else{
                        echo "</br><p>Il n'y a aucun document</p></br>";
                    }
                    ?>
                    
                </tbody>
                </table>

                <div class="d-flex justify-content-center">
                    <button class="btnSecond text-center" id="show_more">Voir plus</button>
                </div>
            </div>
        </div>
    </div>
</main>


<script>
    var tags = document.querySelectorAll(".tags button");
    var offset_recherche = 10;

    tags.forEach(tag => {
        tag.addEventListener("click", () => {
            tag.classList.toggle("clicked");

        });
    });

    var show_more = document.getElementById("show_more");

    // console.log("toto")
    show_more.addEventListener("click", () => {
        var tbody = document.querySelector("tbody");
        var tr_tab = document.querySelector("tbody tr");
        var param_recherche = document.querySelector("#param_recherche");
        var url_api;

        if(param_recherche){
            url_api = "<?php echo $url ?>/find/" + param_recherche.innerHTML + "/" + offset_recherche;
        }else{
            url_api = "<?php echo $url ?>/documents/offset/" + offset_recherche;
        }

        fetch(url_api)
            .then(function (response) {
                    response.json().then((data) => {
                        console.log(data)

                        data.forEach(e => {
                            let new_tr = tr_tab.cloneNode(true);


                            new_tr.querySelector("#tab_nom").textContent = e.nom;
                            new_tr.querySelector("#tab_chemin").textContent = e.chemin;
                            new_tr.querySelector("#tab_taille").textContent = e.size + " ko";
                            // new_tr.querySelector("#tab_modifier") = e.nom;
                            // new_tr.querySelector("#tab_telecharger") = e.nom;

                            // console.log(new_tr);

                            // tbody.insertAdjacentElement("beforeend", new_tr);
                            tbody.appendChild(new_tr);
                        })

                        offset_recherche += 10;
                    })
                })
    })
</script>

<script src="../JS/arborescence.js"></script>
<script src="../JS/tags.js"></script>

<?php include('footer.php'); ?>