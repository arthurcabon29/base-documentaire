<?php include('header.php'); ?>



<main class="container">

    <h2 class="text-center fw-bold py-5">Ajouter un utilisateur</h2>

    <!-- ajouter la route vers liste des utilisateurs -->
    <a href="index.php?action" class="btnSecond"><i class="fas fa-chevron-left mx-3"></i>Revenir aux utilisateurs</a>


    <form action="" method="POST" class="my-5">
        
        <div class="form-floating mb-3">
            <input type="nom" class="form-control" id="nom" name="nom" required>
            <label for="nom">Nom</label>
        </div>

        <div class="form-floating mb-3">
            <input type="nom" class="form-control" id="prenom" name="prenom" required>
            <label for="prenom">Prénom</label>
        </div>

        <div class="form-floating mb-3">
            <input type="email" class="form-control" id="Email" name="Email" required>
            <label for="Email">Adresse email</label>
        </div>

        <div class="form-floating mb-3">
            <input type="password" class="form-control" id="pswd" name="Password" required><i onclick="pswdVisible(1)" id="eye1" class="toggle-pwd far fa-eye-slash"></i>
            <label for="Password">Mot de passe</label>
        </div>

        <div class="form-floating mb-3">
            <input type="password" class="form-control" id="pswdConfirm" name="ConfirmPassword" required><i onclick="pswdVisible(2)" id="eye2" class="toggle-pwd far fa-eye-slash"></i>
            <label for="ConfirmPassword">Confirmation de mot de passe</label>
        </div>

        <input type="checkbox" name="admin">
        <label for="amdin">Cet utilisateur aura des droits d'administration sur les documents</label>

    </form>

    <button class="btnMain btnValidation" href="">Enregistrer l'utilisateur</button>

</main>

<script>
    /* --- Visibilité mot de passe --- */
    function pswdVisible(param) {

        switch (param) {
            case 1:
                var champ = document.getElementById("pswd");
                var eye = document.getElementById("eye1");

                break;
            case 2:
                var champ = document.getElementById("pswdConfirm");
                var eye = document.getElementById("eye2");

                break;
        }


        if (champ.type === "password") {
            champ.type = "text";
            eye.className = "toggle-pwd far fa-eye";
        } else {
            champ.type = "password";
            eye.className = "toggle-pwd far fa-eye-slash";
        }
    }
</script>

<?php include('footer.php'); ?>