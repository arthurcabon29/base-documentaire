<?php session_start(); ?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../CSS/style.css">
    
    <!-- Icones Font Awesome -->
    <script src="https://kit.fontawesome.com/447c258af7.js" crossorigin="anonymous"></script>
    <title>InfoDocs</title>
</head>

<body>
    <nav class="navbar navbar-light bg-light">

        <div class="container">
            <a class="navbar-brand fst-italic fs-2 fw-bold" href="home.php"><i class="redIcon fas fa-cloud-download-alt mx-2"></i>InfoDocs</a>
            <?php if (isset($_SESSION['notif'])) { echo $_SESSION['notif']; unset($_SESSION['notif']); } ?>

            <?php
                $url = "http://". $_SERVER['SERVER_NAME']; 
                $url .= ":8067";
            ?>

            
            <?php
            if (isset($_SESSION['username'])) { 
                // Si utilisateur connecté, affichage des fonctionnalités liées à son compte ?>
            <div class="dropdown">
                <button class="btn btn-outline-dark dropdown-toggle" type="button" id="dropdownMenuButton1"
                    data-bs-toggle="dropdown" aria-expanded="false">
                    <?php echo $_SESSION['username']; ?>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    
                    <?php 
                    if ($_SESSION['idrole'] == 1) { 
                    
                        // Si l'utilisateur est un admin, affichage des fonctions d'admin en plus des autres ?>
                        
                        <!-- ajouter la route vers liste des utilisateurs -->
                        <li><a class="dropdown-item" href="user_form.php">Ajouter un utilisateur</a></li>
                        <li><a class="dropdown-item" href="document_form.php">Ajouter un document</a></li>
                        <hr>
                    <?php } ?>

                    <li><a class="dropdown-item" href="index.php?action=mesDocs">Documents d'entreprise</a></li>
                    <li><a class="dropdown-item" href="changer_mdp.php">Modifier mon mot de passe</a></li>
                    <li><a class="dropdown-item" href="index.php?action=logout">Déconnexion</a></li>
                </ul>
            </div>

            <?php } else { // Autrement, si utilisateur non authentifié, affichage du bouton connexion ?>
                <div>
                    <a class="btn btn-outline-dark" href="connexion.php"><i class="redIcon fas fa-sign-in-alt"></i> Connexion</a>
                </div>
            <?php } ?>
        </div>
    </nav>
