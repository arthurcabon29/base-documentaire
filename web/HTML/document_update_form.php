<?php 

include('header.php'); ?>

<main class="container">

    <h2 class="text-center fw-bold py-5">Modifiez un document</h2>

    <a class="btnSecond" href="javascript:history.back()"><i class="fas fa-chevron-left mx-3"></i>Revenir aux documents</a>


    <form action="index.php?action=update_document&param=<?php echo $_GET['param'] ?>" method="POST" class="my-5" enctype="multipart/form-data">

        <div id="divVignettes"> </div>

        <div class="mb-3">
            <label for="formFile" class="form-label">Veuillez sélectionner le nouveau dossier à envoyer</label>
            <input class="form-control" type="file" name="formFile" id="formFile" accept=".zip, .tgz" required>
        </div>

        <div class="centerDiv">
            <button class="btnMain btnValidation" name="" type="submit">Enregistrer le nouveau document</button>
        </div>
        
    </form>

</main>

<?php include('footer.php'); ?>